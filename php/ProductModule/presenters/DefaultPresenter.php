<?php

namespace FrontModule\ProductModule;

use App\Forms\AddToBasketDetail\Form as BasketForm;
use App\Forms\AvailabilityQuestion\Form as QuestionForm;
use Components\OldLinksRedirect;
use Components\ProductDetail;
use Components\ProductLabels;
use Nette\Application\BadRequestException;
use Nette\Http\Request;
use Nette\Utils\Callback;
use Nette\Utils\Html;
use Utils\FileDownload;
use Utils\GalleryImage;
use Utils\ProductParameter;
use Utils\ProductSalesInfo;
use Utils\ProductStock;
use Utils\ProductStocks;
use Utils\ProductVariant;

class DefaultPresenter extends \FrontModule\BasePresenter
{

    /** @var ProductModel */
    private $productModel;
    /** @var ProductDetail */
    private $productDetailComp;
    private $product;
    /** @var Request */
    private $httpRequest;
    /** @var OldLinksRedirect */
    private $oldLinksRedirect;

    public function startup(): void
    {
        parent::startup();
        if ($this->frontUtils->isBatteriesWeb()) {
            $link = $this->oldLinksRedirect->directCheck($this->request->parameters['productToken']);
            if (!$link) {
                $link = $this->httpRequest->url->path;
            } else {
                $link = substr($link, 1, strlen($link));
                $link = $this->link('this', [$link]);
            }
            $this->redirectUrl(
                'https://pemacz.cz' . $link,
                301
            );
        }
    }

    public function injectOldLinksRedirect(OldLinksRedirect $comp)
    {
        $this->oldLinksRedirect = $comp;
    }

    public function injectProductModel(ProductModel $model)
    {
        $this->productModel = $model;
    }

    public function injectProductDetailComp(ProductDetail $component)
    {
        $this->productDetailComp = $component;
    }

    public function injectHttpRequest(Request $request)
    {
        $this->httpRequest = $request;
    }

    public function actionDefault($productToken)
    {
        $this->product = $this->productModel->getProductDetail($productToken);
        if (!$this->product) {
            throw new BadRequestException($this->getTranslator()->translate('product.notFound'));
        }
        $this['productDetail']->addToBasketHandler = [$this, 'processAddToBasket'];
        $this['productDetail']->addToBasketFromListHandler = function ($product) {
            call_user_func_array([$this, 'handleToBasketFromList'], $product);
        };

        /** @var ProductDetail */
        $detailComponent = $this['productDetail'];
        $detailComponent->product = $this->product;
        $this->template->product = $this->product;
        $detailComponent->parameters = array_map(
            function ($item) {
                return (new ProductParameter())->create($item->name, $item->value, $item->unit);
            },
            $this->productModel->getProductParams($this->product->id_product, $this->product->subsumption)
        );

        $detailComponent->images = $images = array_map(function ($image) {
            return (new GalleryImage())->create(
                $image->link,
                $image->update ? $image->update->getTimestamp() : null,
                $image->title,
                $image->description,
                $image->type
            );
        }, $this->productModel->getGallery($this->product->id_product));
        $this->template->mainImages = array_filter($images, function ($image) {
            return $image->type === GalleryImage::TYPE_MAIN_IMAGE;
        });
        $siblings = $this->productModel->getProductSiblings($this->product->id_product);
        foreach ($siblings as &$sibling) {
            $sibling->link = $this->link('default', $sibling->link);
        }
        $detailComponent->siblings = $siblings;

        $detailComponent->variants = array_map(function ($item) {
            return (new ProductVariant())
                ->create(
                    $item->id_product,
                    $item->value,
                    $item->abbr,
                    $item->totalPriceVatFormatted(true),
                    $this->link('default', $item->link)
                );
        }, $this->productModel->getVariants($this->product->variation_set));

        $detailComponent->downloads = array_map(function ($item) {
            $link = $item->link;
            if ($item->type !== ProductModel::LINK_TYPE_LINK) {
                $link = $this->getHttpRequest()->url->basePath . $link;
            }
            return (new FileDownload())
                ->create($link, $item->title, $item->description, $item->type);
        }, $this->productModel->getDownloads($this->product->id_product));

        $stocks = array_map(
            function ($stock) {
                return (new ProductStock())
                    ->create($stock->title, $stock->description, $stock->quantity, $stock->stock, $stock->text_meaning);
            },
            $this->productModel->getStocks($this->product->id_product)
        );
        if ($stocks) {
            $detailComponent->stocks = (new ProductStocks())->create($stocks);
        }
        $detailComponent->salesInfo = $this->createSalesInfo();

        /** BREAD CRUMBS AND MENU SETTINGS */
        $category = $this->productModel->getCategoryPath($this->product->id_product);
        if ($category) {
            $menuItems = isset($this['categoryMenu']) && $this['categoryMenu']->categories->items ? $this['categoryMenu']->categories->items : [];
            $this->template->categoryPathText = $this->frontUtils->categoryPathToText($category->path, $menuItems);
        }
        $this['breadCrumbs']->activePath = $category->path ?? null;
        $this['breadCrumbs']->activeProduct = $this->product;
        $this['categoryMenu']->activeCategory = $category->id_category ?? null;
        $this->template->subpage = 1;
        $this->template->productDetail = 1;
        /** END BREAD CRUMBS AND MENU SETTINGS */
    }

    private function createSalesInfo(): array
    {
        $info = [];
        if ($this->product->sales_code) {
            $info[] = (new ProductSalesInfo())->create($this->translator->translate("product.salesCode"), $this->product->sales_code);
        }
        if ($this->product->brand_name) {
            $info[] = (new ProductSalesInfo())->create(
                $this->translator->translate("product.producer"),
                Html::el("a")
                    ->href($this->link(':Front:Brand:Default:', $this->product->brandLink))
                    ->setText($this->product->brand_name)
            );
        }
        $info[] = (new ProductSalesInfo())->create($this->translator->translate("product.producerCode"), $this->product->brand_number ?: '-');
        if ($this->product->warranty_customer) {
            $info[] = (new ProductSalesInfo())->create($this->translator->translate("product.warranty"), $this->product->warranty_customer);
        }
        if ($this->product->warranty_company) {
            $info[] = (new ProductSalesInfo())->create($this->translator->translate("product.warranty_company"), $this->product->warranty_company);
        }
        return $info;
    }

    /**
     * @param \Nette\Application\UI\Form $form
     * @param \Nette\Utils\ArrayHash $values
     */
    public function processAddToBasket(
        \Nette\Application\UI\Form $form,
        \Nette\Utils\ArrayHash $values
    ) {
        $response = $this['basket']->addToBasket($this->product->id_product, $values->count);
        if (is_numeric($response)) {
            $this->flashMessage($this->translator->translate("basketComponent.maximumCountExceeded", $response));
            $this->redrawControl('flashes');
            if (!$this->isAjax())
                $this->redirect('this');
        } elseif (!$this->isAjax()) {
            $this->redirect('this');
        } elseif ($response) {
            $this->payload->basketCount = $this['basket']->getTotalCount();
            $this->payload->basketContent = $this['basket']->renderShort(true);
            $this['modal']->open($this['basket']->getBasketModalContent());
        }
    }

    protected function createComponentProductDetail()
    {
        return $this->productDetailComp->create();
    }
}
