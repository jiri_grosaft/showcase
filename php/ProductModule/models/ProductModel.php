<?php

namespace FrontModule\ProductModule;

use Utils\GalleryImage;

class ProductModel
{

    use \Nette\SmartObject;

    /** @var \Dibi\Connection */
    private $db;
    private $translator;

    /** @var \Detection\MobileDetect */
    private $mobileDetect;

    /** @var \Components\ProductLabels */
    private $labels;

    /** @var \App\Utils\Product\EntityBuilder */
    private $productEntityBuilder;

    /** @var \App\Utils\FrontModelUtils */
    private $modelUtils;

    /** @var \Nette\Http\Request @inject */
    private $httpRequest;

    const NO_SALE_MEANING = 2000000515;
    const NO_SALE_SIBLING = 1516;
    const LINK_TYPE_DOWNLOAD = 4;
    const LINK_TYPE_LINK = 3;
    const STOCK_SHOP = 1;
    const STOCK_ESHOP = 258;
    const STOCK_SUM = 250;
    const GROUP_OIL = 540;
    const GROUP_TOOLS = 1423;
    const GROUP_BATTERIES = 538;
    const GROUP_BATTERIES_MOTO = 528;
    const PRODUCT_SIBLING = 2030003547;
    const STOCK_STATE_DEMAND = 2000000516;
    const STOCK_STATE_UNAVAILABLE = 2000000514;
    const STOCK_STATE_AT_SUPPLIER = 8892;
    const STOCK_STATE_SOLD_OUT = 2000000513;
    const DEPOSIT_ID = 128907;

    const JSON_TLD_AVAILABILITY = [
        2000000510 => "InStock",
        8892 => "InStock",
        2000000511 => "OutOfStock",
        2000000516 => "OutOfStock",
        2000000512 => "OutOfStock",
        2000000513 => "SoldOut",
        2000000514 => "OutOfStock",
        2000000515 => "Discontinued",
    ];

    /**
     * @param \Components\ProductLabels $labels
     * @param \Dibi\Connection $db
     * @param \Components\TranslatorComponent $translator
     * @param \Detection\MobileDetect $mobileDetect
     * @param \App\Utils\Product\EntityBuilder $productEntityBuilder
     * @param \App\Utils\FrontModelUtils $modelUtils
     */
    public function __construct(
        \Components\ProductLabels $labels,
        \Dibi\Connection $db,
        \Components\TranslatorComponent $translator,
        \Detection\MobileDetect $mobileDetect,
        \App\Utils\Product\EntityBuilder $productEntityBuilder,
        \App\Utils\FrontModelUtils $modelUtils,
        \Nette\Http\Request $request
    ) {
        $this->labels = $labels;
        $this->db = $db;
        $this->translator = $translator;
        $this->mobileDetect = $mobileDetect;
        $this->productEntityBuilder = $productEntityBuilder;
        $this->modelUtils = $modelUtils;
        $this->httpRequest = $request;
    }

    /**
     * Get id of product from URL token
     * @param string $token
     * @return string|boolean
     */
    private function getProducIdFromUrlToken(string $token)
    {
        if (!$token)
            return FALSE;
        $arr = explode('_', $token);
        return end($arr);
    }

    /**
     * Return product detail
     * @param string $token
     * @return \Dibi\Row|FALSE
     */
    public function getProductDetail(string $token)
    {
        $id = $this->getProducIdFromUrlToken($token);
        if (!$id)
            return false;
        $langId = $this->translator->getLangId();
        $flagSql = $this->labels->getFlagsForSql('product_feature');
        $isFromFilterGuide = $this->verifyFilterGuideReferrer();
        $result = $this->db->select(
            'product.*, 
            product_locale.*, 
            brand.name as brand_name, 
            warrantyCustomer.description as warranty_customer,
            warrantyCompany.description as warranty_company,
            product_price.*,   
            tsl.abbr as unit' . ($flagSql ? ' ,' . $flagSql : '')
        )
            ->select('product_stock.flag_hide_price AS stock_hide_price')
            ->select('product_stock.flag_not_buy AS stock_not_buy')
            ->from('product')
            ->innerJoin('product_locale')
            ->on('product_locale.id_product = product.id_product')
            ->leftJoin('brand')
            ->on('id_brand = product.brand')
            ->innerJoin('product_stock')
            ->on('product.id_product = product_stock.id_product')
            ->and('product_stock.stock = %i', ProductModel::STOCK_SUM)
            ->leftJoin('text_locale warrantyCustomer')
            ->on('warrantyCustomer.id_text = warranty_customer ')
            ->and('warrantyCustomer.id_language = %i', $langId)
            ->leftJoin('text_locale warrantyCompany')
            ->on('warrantyCompany.id_text = warranty_firm')
            ->and('warrantyCompany.id_language = %i', $langId)
            ->leftJoin('product_price')
            ->on('product_price.id_product = product.id_product')
            ->leftJoin('text_special_locale tsl')
            ->on('tsl.id_text_special = product.unit')
            ->and('tsl.id_language = %i', $langId)
            ->innerJoin('product_feature')
            ->on('product_feature.id_product = product.id_product')
            ->where('product_locale.id_language = %i', $langId)
            ->where('product.id_product = %i', $id)
            ->where(
                'product_locale.id_web = '
                    . 'IF('
                    . 'EXISTS('
                    . 'SELECT id_product FROM product_locale WHERE id_product = product.id_product AND id_web = %s'
                    . '), ' . $this->modelUtils->webId . ', 0'
                    . ')',
                $this->modelUtils->webId
            )
            ->where(
                'product_feature.id_web = '
                    . 'IF('
                    . 'EXISTS('
                    . 'SELECT id_product FROM product_feature WHERE id_product = %s AND id_web = %s'
                    . '), ' . $this->modelUtils->webId . ', 0'
                    . ')',
                $id,
                $this->modelUtils->webId
            );
        $product = ($result->fetch());
        return $this->productEntityBuilder->create($product);
    }

    private function verifyFilterGuideReferrer()
    {
        $translations = \App\RouterFactory::TRANSLATIONS[$this->translator->getLang()] ?? [];
        if (!$translations) {
            return false;
        }
        $moduleName = $translations['module']['FiltersGuide'] ?? null;
        if (!$moduleName) {
            return false;
        }
        $referer = $this->httpRequest->referer;
        return stripos($referer, $moduleName) !== false;
    }

    /**
     * Return product parameters for table
     * @param int $idProduct
     * @param int|null $subsumption
     * @return array Array contains \Dibi\Row[value, name, unit]
     */
    public function getProductParams($idProduct, ?int $subsumption = null): array
    {
        $idLang = $this->translator->getLangId();
        $query = $this->db->select('            
            ppl.value as value,
            pl.name as name,
            tsl.title as unit
            ')
            ->from('product_parameter pp')
            ->innerJoin('parameter_locale pl')
            ->on('pp.id_parameter = pl.id_parameter')
            ->and('pl.id_language = %i', $idLang)
            ->leftJoin('text_special_locale tsl')
            ->on('pp.unit = tsl.id_text_special')
            ->and('tsl.id_language = %i', $idLang)
            ->innerJoin('product_parameter_locale ppl')
            ->on('pp.value = ppl.id_value');
        if ($subsumption !== null) {
            $query->innerJoin('parameter_subsumption ps')
                ->on('ps.parameter = pp.id_parameter')
                ->and('id_subsumption = %i', $subsumption);
        }
        $query->where('pp.id_product = %i', $idProduct)
            ->where('pl.type = 1')
            ->where('pp.for_detail = 1');
        if ($subsumption !== null) {
            $query->orderBy('parameter_order');
        }
        return ($query->fetchAll());
    }

    /**
     * @param int|null $idProduct
     * @return array
     */
    public function getGallery(?int $idProduct): array
    {
        if (!$idProduct) {
            return [];
        }
        $images = $this->db->select([
            'type',
            'title',
            'description',
            'link',
            'product_link.update',
        ])->from('product_link')
            ->innerJoin('product_link_locale')
            ->using('(id_product_link)')
            ->where('id_product = %i', $idProduct)
            ->where('id_language = %i', $this->translator->getLangId())
            ->where('type IN %in', [GalleryImage::TYPE_MAIN_IMAGE, GalleryImage::TYPE_THUMB_IMAGE])
            ->fetchAll();
        return $images;
    }

    /**
     * @param mixed $idProduct
     * @return \Dibi\Row|false
     */
    public function getCategoryPath($idProduct)
    {
        return $this->db->select('path,id_category')
            ->from('product p')
            ->innerJoin('product_category pc')
            ->on('p.id_product = pc.product')
            ->or('(p.product_group = pc.product_group')
            ->and("p.product_group != 0)")
            ->where('p.id_product = %i', $idProduct)
            ->where('id_web IN %in', [$this->modelUtils->getWebId(), 0])
            ->fetch();
    }

    /**
     * @param int|null $idProduct
     * @return array
     */
    public function getProductSiblings(?int $idProduct): array
    {
        if (!$idProduct) {
            return [];
        }
        $args = [
            'p.id_product',
            'netto',
            'brutto',
            'pl.name',
            'pll.link' => 'image',
            'pll.title' => "imageTitle",
            'stock_title.title' => 'stock',
            'pstock.quantity' => 'stock_count',
            "text_meaning" => "stock_state",
        ];
        $flagSql = $this->labels->getFlagsForSql('product_feature');
        $query = $this->db->select($args)
            ->select(($flagSql ?: ''))
            ->from('product_sibling ps')
            ->innerJoin('product p')
            ->on('p.id_product = ps.product_sibling')
            ->and('ps.type = %i', self::PRODUCT_SIBLING)
            ->innerJoin('product_price pp')
            ->on('pp.id_product = p.id_product')
            ->innerJoin('product_locale pl')
            ->on('p.id_product = pl.id_product')
            ->and(
                'pl.id_web = '
                    . 'IF('
                    . 'EXISTS('
                    . 'SELECT id_product FROM product_locale WHERE id_product = p.id_product AND id_web = %s'
                    . '), ' . $this->modelUtils->webId . ', 0'
                    . ')',
                $this->modelUtils->webId
            )
            ->leftJoin('product_link')
            ->on('product_link.id_product = p.id_product')
            ->and('product_link.type = %i', GalleryImage::TYPE_MAIN_IMAGE)
            ->leftJoin('product_link_locale pll')
            ->on('pll.id_product_link = product_link.id_product_link')
            ->leftJoin('product_stock pstock')
            ->on('p.id_product = pstock.id_product')
            ->and('stock = %i', self::STOCK_SUM)
            ->leftJoin('text_locale AS stock_title')
            ->on('id_text = stock_text')
            ->innerJoin('product_feature')
            ->on('product_feature.id_product = ps.product_sibling')
            ->and(
                'product_feature.id_web = '
                    . 'IF('
                    . 'EXISTS('
                    . 'SELECT id_product FROM product_feature WHERE id_product = ps.product_sibling AND id_web = %s'
                    . '), ' . $this->modelUtils->webId . ', 0'
                    . ')',
                $this->modelUtils->webId
            )
            ->and('product_feature.flag_hide = 0')
            ->and('product_feature.flag_not_buy = 0')
            ->where('ps.id_product = %i', $idProduct)
            ->orderBy('ps.order');
        return $this->productEntityBuilder->create($query->fetchAssoc('id_product'));
    }

    /**
     * @param type $idVariant
     * @return array
     */
    public function getVariants($idVariant)
    {
        if (!$idVariant) {
            return [];
        }
        $query = $this->db->select('pal.name as variant_name,netto,brutto,p.id_product, abbr, ppl.value, pl.name')
            ->from('product p')
            ->innerJoin('product_locale pl')
            ->on('p.id_product = pl.id_product')
            ->and(
                'pl.id_web = '
                    . 'IF('
                    . 'EXISTS('
                    . 'SELECT id_product FROM product_locale WHERE id_product = p.id_product AND id_web = %s'
                    . '), ' . $this->modelUtils->webId . ', 0'
                    . ')',
                $this->modelUtils->webId
            )
            ->innerJoin('product_price pp')
            ->on('pp.id_product = p.id_product')
            ->innerJoin('product_parameter prp')
            ->on('prp.id_product = p.id_product')
            ->innerJoin('parameter_locale pal')
            ->using('(id_parameter)')
            ->innerJoin('parameter_subsumption ps')
            ->on('ps.parameter = pal.id_parameter')
            ->and("for_variation = 1")
            ->innerJoin('product_parameter_locale ppl')
            ->on('ppl.id_value = prp.value')
            ->leftJoin('text_special_locale tsl')
            ->on('prp.unit = tsl.id_text_special')
            ->and('tsl.id_language = %i', $this->translator->getLangId())
            ->where('pal.id_language = %i', $this->translator->getLangId())
            ->where('variation_set = %i', $idVariant)
            ->groupBy('id_product')
            ->orderBy('netto,value')
            ->asc()
            ->fetchAll();
        return $this->productEntityBuilder->create($query);
    }

    /**
     * @param int $idProduct
     * @return array
     */
    public function getNoSaleSibling(int $idProduct)
    {
        $args = [
            'p.id_product',
            'netto',
            'brutto',
            'pl.name',
            'pll.link' => 'image',
            'pll.title' => "imageTitle",
        ];
        $query = $this->db->select($args)
            ->from('product_sibling ps')
            ->innerJoin('product p')
            ->on('p.id_product = ps.product_sibling')
            ->innerJoin('product_price pp')
            ->on('pp.id_product = p.id_product')
            ->innerJoin('product_locale pl')
            ->on('p.id_product = pl.id_product')
            ->and(
                'pl.id_web = '
                    . 'IF('
                    . 'EXISTS('
                    . 'SELECT id_product FROM product_locale WHERE id_product = p.id_product AND id_web = %s'
                    . '), ' . $this->modelUtils->webId . ', 0'
                    . ')',
                $this->modelUtils->webId
            )
            ->leftJoin('product_link')
            ->on('product_link.id_product = p.id_product')
            ->and('product_link.type = %i', GalleryImage::TYPE_MAIN_IMAGE)
            ->leftJoin('product_link_locale pll')
            ->on('pll.id_product_link = product_link.id_product_link')
            ->where('ps.id_product = %i', $idProduct)
            ->where('ps.type = %i', self::NO_SALE_SIBLING)
            ->orderBy('ps.order');
        return $this->productEntityBuilder->create($query->fetch());
    }

    /**
     * @param int|null $idProduct
     * @return array
     */
    public function getDownloads(?int $idProduct): array
    {
        if (!$idProduct) {
            return [];
        }
        return $this->db->select('link, title, description, type')
            ->from('product_link')
            ->innerJoin('product_link_locale')
            ->using('(id_product_link)')
            ->where(
                'type IN %in',
                [self::LINK_TYPE_DOWNLOAD, self::LINK_TYPE_LINK]
            )
            ->where(
                'id_language = %i',
                $this->translator->getLangId()
            )
            ->where('id_product = %i', $idProduct)
            ->fetchAll();
    }

    /**
     * @return array
     */
    public function getDeliveries(): array
    {
        return $this->db->select('dl.*, pp.netto, pp.brutto')
            ->from('delivery d')
            ->innerJoin('product_price pp')
            ->on('d.product = pp.id_product')
            ->innerJoin('delivery_locale dl')
            ->using('(id_delivery)')
            ->innerJoin('delivery_payment dp')
            ->on('dp.id_delivery = d.id_delivery')
            ->and('id_web = %i', $this->modelUtils->webId)
            ->where(
                'dl.id_language = %i',
                $this->translator->getLangId()
            )
            ->where(
                'pp.currency = %s',
                $this->translator->getCurrency()->currency
            )
            ->where('require_info = 1')
            ->fetchAssoc('id_delivery');
    }

    /**
     * @param int|null $idDelivery
     * @return \Dibi\Row|false
     */
    public function getDelivery($idDelivery)
    {
        return $this->db->select('d.*, dl.*, pp.*')
            ->from('delivery d')
            ->innerJoin('product_price pp')
            ->on('d.product = pp.id_product')
            ->innerJoin('delivery_locale dl')
            ->using('(id_delivery)')
            ->where(
                'dl.id_language = %i',
                $this->translator->getLangId()
            )
            ->where(
                'pp.currency = %s',
                $this->translator->getCurrency()->currency
            )
            ->where('d.id_delivery = %i', $idDelivery)
            ->fetch();
    }

    /**
     * @param type $idProduct
     * @return array
     */
    public function getStocks($idProduct): array
    {
        return $this->db->select('title,stock,description,quantity, text_meaning')
            ->from('product_stock')
            ->innerJoin('text_locale')
            ->on('id_text = stock_text')
            ->where('id_product = %i', $idProduct)
            ->fetchAssoc('stock');
    }

    /**
     * @param int $price
     * @return numeric
     */
    public function getMinDeliveryPrice(int $price = 0)
    {
        $prices = $this->db->select('MIN(pp.brutto) as brutto, MIN(free_from_price) as free_from_price')
            ->from('delivery d')
            ->innerJoin('product_price pp')
            ->on('d.product = pp.id_product')
            ->where(
                'pp.currency = %s',
                $this->translator->getCurrency()->currency
            )
            ->where('require_info = 1')
            ->fetch();
        if (!$prices)
            return 0;
        if ($price > $prices->free_from_price)
            return 0;
        return $prices->brutto;
    }

    /**
     * @return int|false
     */
    public function getValueForFreeDelivery()
    {
        return $this->db->select('MIN(free_from_price) as free')
            ->from('delivery')
            ->where('require_info = 1')
            ->fetchSingle();
    }
}
