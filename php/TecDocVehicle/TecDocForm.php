<?php

namespace App\Forms\TecDocVehicle;

use \Nette\Utils\Strings;
use \App\Forms\TecDocVehicle\TecDocVehicleModel;
use App\Utils\FrontModelUtils;
use App\Utils\TecDoc\TecDocService;
use Components\CookiesConsentComponent;
use Nette\Http\Request;
use Nette\Http\Response;
use Nette\Utils\ArrayHash;
use stdClass;

/**
 * @property mixed $vehicle
 */
class TecDocForm extends \Nette\Application\UI\Control
{

    /** @var \Components\TranslatorComponent */
    private $translator;

    /** @var TecDocVehicleModel */
    private $tecDocVehicleModel;

    /** @var int @persistent */
    public $manufacturer = null;

    /** @var int @persistent */
    public $model = null;

    /** @var string @persistent */
    public $type = null;

    const SESSION_NAMESPACE = "carPartsVehicle";
    const SESSION_EXPIRATION = "14 days";

    /** @var FrontModelUtils */
    private $utils;

    /** @var TecDocService */
    private $tecDocService;

    /** @var Request */
    private $request;

    /** @var Response */
    private $response;

    /**
     * Order of items is crucial, it define dependencies in form selection steps, 0 - is first, etc...
     * @var array
     */
    private $formFields = ['type', 'manufacturer', 'model'];

    /** @var CookiesConsentComponent */
    private $cookiesConsent;

    /** @var ArrayHash */
    private $cookie;

    public function __construct(
        \Components\TranslatorComponent $translator,
        TecDocVehicleModel $model,
        FrontModelUtils $utils,
        TecDocService $tecDocService,
        CookiesConsentComponent $cookiesConsent,
        Request $request,
        Response $response
    ) {
        $this->translator = $translator;
        $this->tecDocVehicleModel = $model;
        $this->utils = $utils;
        $this->tecDocService = $tecDocService;
        $this->cookiesConsent = $cookiesConsent;
        $this->request = $request;
        $this->response = $response;
        if (!$this->cookiesConsent->getSetup(CookiesConsentComponent::CONSENT_PREFERENCES)) {
            $this->resetVehicleSelection();
        }
    }

    public function getVehicle()
    {
        $cookie = $this->request->getCookie(self::SESSION_NAMESPACE);
        if (!$cookie) {
            return 0;
        } else if ($cookie) {
            $parsed = json_decode($cookie);
            return $parsed instanceof stdClass ? ($parsed->vehicle ?? 0) : 0;
        };
    }

    public function setVehicle(stdClass $vehicle)
    {
        $vehicle->link  = $this->buildLink($vehicle);
        $cookie = $this->request->getCookie(self::SESSION_NAMESPACE);
        if ($cookie) {
            $parsed = json_decode($cookie);
            if ($parsed instanceof stdClass) {
                $parsed->vehicle = $vehicle;
            }
            $this->response->setCookie(self::SESSION_NAMESPACE, json_encode($parsed), self::SESSION_EXPIRATION);
        }
    }

    public function render()
    {
        $tmpl = $this->template
            ->setFile(__DIR__ . '/template.latte')
            ->setTranslator($this->translator);
        $this->setupType();
        $this->setupManufacturer();
        $this->setupModel();
        $this->allowFormSubmit();
        $tmpl->vehiclesList = $this->getVehicleList();
        $tmpl->type = $this->type;
        $tmpl->vehicleDetail = $this->vehicle;
        $name = $this->vehicle ? $this->tecDocService->generateVehicleNameFromData($this->vehicle) : null;
        $tmpl->vehicleName = $name;
        $tmpl->vehicleLink = $this->vehicle ? $this->utils->tecDocVehicleLink($name, $this->vehicle->carId) : null;
        $tmpl->analyticsCookies = $this->cookiesConsent->getSetup(CookiesConsentComponent::CONSENT_ANALYTICS);
        $tmpl->render();
    }

    /**
     * @return \App\Forms\TecDocVehicle\Form
     */
    public function create(): TecDocForm
    {
        return $this;
    }

    private function getVehicleList(): array
    {
        if (!$this->model) {
            return [];
        }
        return $this->tecDocVehicleModel->getVehicles($this->manufacturer, $this->model);
    }

    /**
     * @return \Nette\Application\UI\Form
     */
    protected function createComponentForm(): \Nette\Application\UI\Form
    {
        $form = new \Nette\Application\UI\Form();
        $searchPlaceholder = Strings::firstUpper($this->translator->translate("search"));
        foreach ($this->formFields as $field) {
            $form->addSelect(
                $field,
                $this->translator->translate("tecDocForm.$field"),
                $field === 'type' ? $this->tecDocVehicleModel->getTypes() : []
            )
                ->setHtmlAttribute('data-placeholder', $searchPlaceholder)
                ->setHtmlAttribute("data-search")
                ->setHtmlAttribute('data-ajax-postByChange')
                ->setHtmlAttribute('data-allForm')
                ->setHtmlAttribute('data-autofocus')
                ->setHtmlAttribute('data-w-100')
                ->setPrompt($this->translator->translate('tecDocForm.' . $field . 'Prompt'))
                ->setDisabled($field !== 'type');
            if ($field === 'type') $form[$field]->setHtmlAttribute('data-moto-image');
        }
        $form->addSubmit('send', 'tecDocForm.send')
            ->setDisabled(false);
        $form->onSuccess[] = [$this, 'submitForm'];
        return $form;
    }

    /**
     * @param \Nette\Application\UI\Form $form
     * @return void
     */
    public function submitForm(\Nette\Application\UI\Form $form): void
    {
        $values = $form->getHttpData();
        $this['form']->reset();
        $resetNext = false;
        foreach ($this->formFields as $field) {
            if ($resetNext) {
                $this->{$field} = null;
                continue;
            }
            if (isset($values[$field]) && $this->{$field} != $values[$field]) {
                $resetNext = true;
            }
            if (isset($values[$field]) && !empty($values[$field])) {
                $this->{$field} = $values[$field];
            }
        }
        if ($this->presenter->isAjax()) {
            $this->presenter->payload->url = $this->link('this');
            $this->redrawControl('form');
            $this->redrawControl('list');
        } else {
            $this->redirect('this');
        }
    }

    private function setupType()
    {
        $field = 'type';
        $types = $this['form'][$field]->items;
        if (isset($types[$this->type])) {
            $this['form'][$field]->setHtmlAttribute('data-placeholder', Strings::firstUpper($this->translator->translate("search")));
            $this['form'][$field]->setDefaultValue($this->type);
        }
    }

    private function setupManufacturer()
    {
        $manufacturers = $this->tecDocVehicleModel->getManufacturers($this->type);
        $field = 'manufacturer';
        $this['form']['manufacturer']->setItems($manufacturers);
        if ($manufacturers) {
            $this['form'][$field]->setDisabled(false);
            $this['form'][$field]->setPrompt($this->translator->translate('tecDocForm.' . $field . 'PromptActive'));
        }
        if ($this->manufacturer && isset($manufacturers[$this->manufacturer])) {
            $this['form'][$field]->setDefaultValue($this->manufacturer);
        } else {
            $this['form'][$field]->setDefaultValue(null);
        }
    }

    private function setupModel()
    {
        $field = 'model';
        $models = $this->tecDocVehicleModel->getModels($this->type, $this->manufacturer);
        $this['form'][$field]->setItems($models);
        if ($models) {
            $this['form'][$field]->setDisabled(false);
            $this['form'][$field]->setPrompt($this->translator->translate('tecDocForm.' . $field . 'PromptActive'));
        }
        if ($this->model && isset($models[$this->model])) {
            $this['form'][$field]->setDefaultValue($this->model);
        } else {
            $this['form'][$field]->setDefaultValue(null);
        }
    }

    private function allowFormSubmit()
    {
        $allFields = true;
        foreach ($this->formFields as $field) {
            if (!$this['form'][$field]->value) {
                $allFields = false;
                break;
            }
        }
        if ($allFields) {
            $this['form']['send']->setDisabled(false);
        } else {
            $this['form']['send']->setDisabled();
        }
    }

    public function resetVehicleSelection()
    {
        $this->response->deleteCookie(self::SESSION_NAMESPACE);
    }

    public function handleRedirectVehicle($vehicle)
    {
        $response = $this->tecDocVehicleModel->getVehicleDetail($vehicle);
        $vehicleDetail = $response[$vehicle] ?? null;
        if ($vehicleDetail) {
            $vehicleDetail->type = $this->type;
            $vehicleDetail->link = $this->buildLink($vehicleDetail);
            if ($this->cookiesConsent->getSetup(CookiesConsentComponent::CONSENT_PREFERENCES)) {
                $this->response->setCookie(self::SESSION_NAMESPACE, json_encode($vehicleDetail), self::SESSION_EXPIRATION);
            }
            $this->presenter->redirect(':Front:TecDoc:Default:catalog', [
                'vehicle' => $vehicleDetail->link
            ]);
        } else {
            $this->redirect('this');
        }
    }

    private function buildLink(stdClass $vehicleData)
    {
        if (!$vehicleData) {
            return null;
        }
        $name = $this->tecDocService->generateVehicleNameFromData($vehicleData);
        return $this->utils->tecDocVehicleLink($name, $vehicleData->carId);
    }
}
