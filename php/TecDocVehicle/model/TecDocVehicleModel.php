<?php

namespace App\Forms\TecDocVehicle;

use App\Utils\TecDoc\Config;

class TecDocVehicleModel
{

    /** @var \Dibi\Connection */
    private $db;

    /** @var \App\Utils\FrontModelUtils */
    private $utils;

    /** @var \App\Utils\TecDoc\TecDocService  */
    private $tecDocService;

    /** @var \Components\TranslatorComponent */
    private $translator;

    public function __construct(
            \Dibi\Connection $db, \App\Utils\FrontModelUtils $utils,
            \App\Utils\TecDoc\TecDocService $tecDocService,
            \Components\TranslatorComponent $translator
    )
    {
        $this->db = $db;
        $this->utils = $utils;
        $this->tecDocService = $tecDocService;
        $this->translator = $translator;
    }

    public function getTypes()
    {
        return [
            Config::TYPE_PASSENGER_CAR => $this->translator->translate("tecDocForm.types.passenger"),
            Config::TYPE_COMMERCIAL_VEHICLE => $this->translator->translate("tecDocForm.types.commercial"),
            Config::TYPE_MOTORBIKE => $this->translator->translate("tecDocForm.types.motorbike"),
        ];
    }

    /**
     * @param string|null $type
     * @return array
     */
    public function getManufacturers(?string $type)
    {
        if (!$type) {
            return [];
        }
        return array_map(function($item) {
            $name = str_replace(" MOTORCYCLES", "", $item);
            return strlen($name) > 3 ? \Nette\Utils\Strings::capitalize($name) : $name;
        }, $this->tecDocService->getManufacturers($type)
        );
    }

    /**
     * @param string|null $type
     * @param int|null $manufacturer
     * @return array
     */
    public function getModels(?string $type, ?int $manufacturer)
    {
        if (!$type || !$manufacturer) {
            return [];
        }
        return $this->tecDocService->getModelSeries($manufacturer, $type);
    }

    /**
     * @param int|null $manufacturer
     * @param int|null $model
     * @return array
     */
    public function getVehicles(?int $manufacturer, ?int $model)
    {
        if (!$manufacturer || !$model) {
            return [];
        }
        return $this->tecDocService->getMotorization($manufacturer, $model);
    }

    /**
     * @param int|null $idVehicle
     * @return array
     */
    public function getVehicleDetail(?int $idVehicle)
    {
        if (!$idVehicle) {
            return [];
        }
        return $this->tecDocService->getVehiclesDetail($idVehicle);
    }

}
