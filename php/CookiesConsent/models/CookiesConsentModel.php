<?php

namespace Components;

use Nette\Http\Request;

class CookiesConsentModel
{

    use \Nette\SmartObject;

    /** @var \Dibi\Connection */
    private $db;

    /** @var \App\Utils\FrontModelUtils */
    private $utils;

    /** @var Request */
    private $request;

    public function __construct(
        \Dibi\Connection $db,
        \App\Utils\FrontModelUtils $utils,
        Request $request
    ) {
        $this->db = $db;
        $this->utils = $utils;
        $this->request = $request;
    }

    public function saveData(array $consentData)
    {
        $values = $consentData;
        $values['ip_address'] = $this->request->getRemoteAddress();
        $values['id_web'] = $this->utils->webId;
        return $this->db->insert('cookies_consent', $values)->execute();
    }
}
