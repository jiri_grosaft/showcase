<?php

namespace Components;

use App\Utils\FrontModelUtils;
use FrontModule\BasketModule\BasketModel;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IComponent;
use Nette\Http\Request;
use Nette\Http\Session;
use Nette\Utils\ArrayHash;

/**
 * @property-read bool $hide
 */
class CookiesConsentComponent extends Control
{

    /** @var TranslatorComponent */
    private $translator;
    /** @var Request */
    private $request;

    private $consentSetup = [];

    private $hide = false;

    const CONSENT_ANALYTICS = 'analytic';
    const CONSENT_ADVERT = 'marketing';
    const CONSENT_PREFERENCES = 'preferences';
    const CONSENT_TYPES = [
        self::CONSENT_ADVERT,
        self::CONSENT_ANALYTICS,
        self::CONSENT_PREFERENCES,
    ];
    /** @var FrontModelUtils */
    private $utils;
    /** @var CookiesConsentModel */
    private $model;

    private $noRender = false;

    /**
     * @param TranslatorComponent $translator
     * @param Request $request
     * @param Session $session
     * @param FrontModelUtils $utils
     * @param CookiesConsentModel $model
     */
    public function __construct(
        TranslatorComponent $translator,
        Request $request,
        Session $session,
        FrontModelUtils $utils,
        CookiesConsentModel $model
    ) {
        $this->translator = $translator;
        $this->request = $request;
        $this->utils = $utils;
        $this->model = $model;
        $sessConsentSetup = $session->getSection('cookiesConsent');
        if (!isset($sessConsentSetup->setup)) {
            $sessConsentSetup->setup = [];
        }
        $this->consentSetup = &$sessConsentSetup->setup;
        $sessHideConsent = $session->getSection('showCookiesConsent');
        if (!isset($sessHideConsent->hide)) {
            $sessHideConsent->hide = false;
        }
        $this->hide = &$sessHideConsent->hide;
    }

    public function getHide(): bool
    {
        return $this->hide;
    }

    public function render()
    {
        if ($this->noRender) {
            return;
        }
        $this->template->setFile(__DIR__ . '/template.latte');
        $this->template->setTranslator($this->translator);
        $this->template->hide = $this->hide;
        $this->template->personalDataLink = $this->presenter->link(':Front:Article:Default:', BasketModel::PROCESS_PERSONAL_DATA_LINKS[$this->utils->webId]);
        $this->template->render();
    }

    protected function createComponentConsentForm()
    {
        $form = new Form();
        $form->setHtmlAttribute('data-ajax');
        foreach (self::CONSENT_TYPES as $type) {
            $checkbox = $form->addCheckbox($type, "cookiesConsent." . $type . "H2");
            $checkbox->setDefaultValue($this->consentSetup[$type] ?? false);
        }
        $form->addSubmit('save', 'cookiesConsent.allowSelected');
        $form->onSuccess[] = [$this, 'saveForm'];
        $form->setTranslator($this->translator);
        return $form;
    }

    public function saveForm(Form $form, ArrayHash $values)
    {
        $this->hide = true;
        $this->consentSetup = (array)$values;
        $this->model->saveData($this->consentSetup);
        $this->presenter->redirect('this');
    }

    public function create()
    {
        return clone $this;
    }

    public function handleAllowAll()
    {
        $this->hide = true;
        foreach (self::CONSENT_TYPES as $type) {
            $this->consentSetup[$type] = true;
        }
        $this->model->saveData($this->consentSetup);
        $this->redirect('this');
    }

    /**
     * Return setup for desired type or complete consent setup
     * @param string|null $type
     * @return array|boolean
     */
    public function getSetup(?string $type = null)
    {
        if ($type) {
            return $this->consentSetup[$type] ?? false;
        } else {
            return $this->consentSetup;
        }
    }

    public function show()
    {
        $this->hide = false;
        $this->redrawControl();
    }

    public function disableRender()
    {
        $this->noRender = true;
    }
}
