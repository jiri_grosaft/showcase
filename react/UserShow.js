import React, { Component } from 'react';
import { Button, Grid } from "@material-ui/core";
import userService from '../services/users.service';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import {
    Show, SimpleShowLayout, translate, EditButton
} from 'react-admin';
import {
    showBusyIndicator as showBusyIndicatorAction,
    showNotifyMessage as showNotifyMessageAction
} from '../../actions/uiActions';
import { reset } from 'redux-form'
import { PrettyNotifyStyle } from '../../components';
import PlusIcon from '@material-ui/icons/Add';
import { refreshView as refreshViewAction } from 'ra-core';
import CliniciansList from '../components/CliniciansList';
import ClinicianDialog from '../components/ClinicianDialog';

class UserShow extends Component {

    initState = {
        clinicians: [],
        cliniciansLoading: false,
        cliniciansLoaded: false,
        userModal: false,
    }

    constructor(props) {
        super(props);
        userService.setTranslator(props.translate);
        this.state = JSON.parse(JSON.stringify(this.initState));
    }

    pageTitle = props => {
        const { record } = props;
        let relatedParty = record && record.relatedParty ? record.relatedParty.find(item => item.role && item.role.toLowerCase() === 'institution') : 0;
        return relatedParty ? relatedParty.name : null;
    }

    usersList = props => {
        if (!props.record || !props.record.institution) {
            return null;
        }
        const { cliniciansLoading, cliniciansLoaded } = this.state;
        const { showBusyIndicator, showNotifyMessage, translate } = this.props;
        if (!cliniciansLoading && !cliniciansLoaded) {
            showBusyIndicator(true);
            userService.getClinicianList(props.record.institution.username)
                .then(response => {
                    this.setState({
                        clinicians: response.json,
                    });
                })
                .catch(error => {
                    console.error(`Unable to load clinicians for institution ${props.record.institution.username}, Error: ${JSON.stringify(error)}`);
                    showNotifyMessage(translate('resources.usersDataForm.clinicianErrorLoading'), PrettyNotifyStyle.error);
                })
                .finally(() => {
                    showBusyIndicator(false);
                    this.setState({
                        cliniciansLoading: false,
                        cliniciansLoaded: true
                    });
                });
            this.setState({ cliniciansLoading: true });
        }
        return <CliniciansList clinicians={this.state.clinicians} record={props.record} {...this.props} />;

    }


    actions = props => {
        const { translate } = this.props;
        return (
            <div style={{ textAlign: 'right' }}>
                <Button color="primary" onClick={() => this.setState({ userModal: true })}>
                    <PlusIcon />
                    {translate('resources.usersDataForm.createUser')}
                </Button>
                <EditButton {...props} record={props.data} />
            </div>
        );
    }

    closeUserDialog(settings) {
        if (settings && settings.reload) {
            const { showBusyIndicator } = this.props;
            showBusyIndicator(true);
            window.location.reload();
        } else {
            this.setState({
                userModal: false
            });
        }
    }

    institutionInfo = props => {
        const { translate } = this.props;
        const { record } = props;
        if (!record || !record.relatedParty) {
            return null;
        }
        let relatedParty = record.relatedParty.find(item => item.role && item.role.toLowerCase() === record.role);
        if (!relatedParty) {
            return null;
        }
        let gridSpancing = 40;
        return (
            <div>
                <Grid container spacing={gridSpancing}>
                    <Grid item xs={12}>
                        <h2 style={{ margin: 0, padding: 0 }}>{translate('resources.usersDataForm.fields.institutionInfo')}</h2>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <strong>{translate('resources.usersDataForm.fields.customerName')}</strong><br />
                        {relatedParty.name}
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <strong>{translate('resources.usersDataForm.fields.accountEmail')}</strong><br />
                        {relatedParty.accountEmail}
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <strong>{translate('resources.usersDataForm.fields.telephoneNumber')}</strong><br />
                        {relatedParty.telephone}
                    </Grid>
                </Grid>
                <Grid container spacing={gridSpancing}>
                    <Grid item xs={12} sm={4}>
                        <p style={{ fontSize: '18px' }}><strong>{translate('resources.usersDataForm.fields.settings')}</strong></p>
                        <strong>{translate('resources.usersDataForm.fields.language')}</strong><br />
                        {userService.langs(relatedParty.language)}<br /><br />
                        <strong>{translate('resources.usersDataForm.fields.currency')}</strong><br />
                        {userService.currencies(relatedParty.billing.currency)}<br /><br />
                        <strong>{translate('resources.usersDataForm.fields.salesPartner')}</strong><br />
                        {userService.salesPartners(relatedParty.salesPartner || false)}<br /><br />
                        <strong>{translate('resources.usersDataForm.fields.stripeAccount')}</strong><br />
                        {userService.stripeAccounts(relatedParty.billing.stripeAccount)}<br /><br />
                        <strong>{translate('resources.usersDataForm.fields.taxExempt')}</strong><br />
                        {userService.taxStatus(relatedParty.billing.taxExempt)}<br /><br />
                        {relatedParty.billing.taxIdType &&
                            <div>
                                <strong>{translate('resources.usersDataForm.fields.taxIdType')}</strong><br />
                                {userService.taxIdTypes(relatedParty.billing.taxIdType)}<br /><br />
                            </div>
                        }
                        <strong>{translate('resources.usersDataForm.fields.taxId')}</strong><br />
                        {relatedParty.billing.taxId}
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <p style={{ fontSize: "18px" }}><strong>{translate('resources.usersDataForm.fields.billingInformation')}</strong></p>
                        <strong>{translate('resources.usersDataForm.fields.billingName')}</strong><br />
                        {relatedParty.billing.name || relatedParty.name}<br /><br />
                        <strong>{translate('resources.usersDataForm.fields.billingEmail')}</strong><br />
                        {relatedParty.billing.email}<br /><br />
                        <strong>{translate('resources.usersDataForm.fields.street')}</strong><br />
                        {relatedParty.address.street}<br /><br />
                        <strong>{translate('resources.usersDataForm.fields.streetNumber')}</strong><br />
                        {relatedParty.address.streetNumber}<br /><br />
                        <strong>{translate('resources.usersDataForm.fields.city')}</strong><br />
                        {relatedParty.address.city}<br /><br />
                        <strong>{translate('resources.usersDataForm.fields.postalCode')}</strong><br />
                        {relatedParty.address.postalCode}<br /><br />
                        <strong>{translate('resources.usersDataForm.fields.country')}</strong><br />
                        {relatedParty.address.state}<br /><br />
                        <strong>{translate('resources.usersDataForm.fields.invoiceIssueDate')}</strong><br />
                        {relatedParty.billing.invoiceIssueDate ? userService.getIssueDates(relatedParty.billing.invoiceIssueDate) : null}<br /><br />
                        <strong>{translate('resources.usersDataForm.fields.invoiceDueDate')}</strong><br />
                        {relatedParty.billing.invoiceDueDate ? userService.getDueDates(relatedParty.billing.invoiceDueDate) : null}<br /><br />
                    </Grid>
                </Grid>
            </div>
        );
    }

    render() {
        const {
            showBusyIndicator, showNotifyMessage, reset,
            save, translate, refreshView, ...rest
        } = this.props;
        return (
            <Show {...rest} title={<this.pageTitle />} actions={<this.actions />}>
                <SimpleShowLayout>
                    {this.state.userModal && <ClinicianDialog {...this.props} closeDialog={this.closeUserDialog.bind(this)} />}
                    <hr style={{ margin: '20px 0', width: '100%', border: 'none', height: '1px', background: 'darkgrey' }} />
                    <this.institutionInfo />
                    <hr style={{ margin: '20px 0', width: '100%', border: 'none', height: '1px', background: 'darkgrey' }} />
                    <this.usersList />
                </SimpleShowLayout>
            </Show>
        );
    }
}

const enhance = compose(
    translate
);

export default enhance(connect(null, {
    showBusyIndicator: showBusyIndicatorAction,
    showNotifyMessage: showNotifyMessageAction,
    reset: reset,
    refreshView: refreshViewAction,
})(UserShow));