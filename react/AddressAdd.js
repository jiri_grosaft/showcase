import React, { Component } from "react";
import { connect } from "react-redux";
import compose from "recompose/compose";
import { translate } from "react-admin";
import { Dialog, DialogContent, DialogTitle, Button } from "@material-ui/core";
import { SimpleForm, Toolbar, SaveButton } from "react-admin";
import {
    showBusyIndicator as showBusyIndicatorAction,
    showNotifyMessage as showNotifyMessageAction,
} from "../../actions/uiActions";
import { PrettyNotifyStyle } from "../../components";
import { push as pushAction } from "react-router-redux";
import usersService from "../services/users.service";
import { isAdmin, isUserAdmin } from "../../authClient";
import Inputs from "../forms/Inputs";
const uuidV1 = require("uuid/v1");
import shared from "../../shared";

export const NonInput = React.memo(function NonInput({ children }) {
    return children;
});

class AddressAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            itemType: null,
            record: false,
            data: null,
            isOpen: true,
            itemName: null,
        };
        const { showBusyIndicator } = props;
        showBusyIndicator(true);
    }

    initRecord = () => {
        const { showBusyIndicator, showNotifyMessage } = this.props;
        showBusyIndicator(true);
        let queryParams = shared.getUrlparams()
        usersService
            .getItemTypesByName("address")
            .then((response) => {
                this.setState({ itemType: response.json.values || {} });
                showBusyIndicator(false);
            })
            .catch((error) =>{
                console.error(`Error get item type: ${JSON.stringify(error)}`)
                showNotifyMessage(
                    "resources.usersDataForm.errorPut",
                    PrettyNotifyStyle.error
                );
                showBusyIndicator(false);
            });
        usersService
            .getUserAttributes("deliveryAddress", queryParams.user)
            .then((response) => {
                let attrs = response.json.pop();
                if (attrs && (isAdmin() || isUserAdmin())) {
                    attrs.mutable = true;
                }
                this.setState({ data: attrs || {} });
                showBusyIndicator(false);
            })
            .catch((error) => {
                console.error(
                    `Error get user attributes item: ${JSON.stringify(error)}`
                )
                showNotifyMessage(
                    "resources.usersDataForm.errorPut",
                    PrettyNotifyStyle.error
                );
                showBusyIndicator(false);
            });
    };

    closeDialog = () => {
        this.setState({
            isOpen: false,
        });
        const { onClose } = this.props;
        onClose();
    };

    componentDidMount() {
        this.initRecord();
    }

    saveDialog() {
        const { onSave } = this.props;
        onSave(this.state.itemName);
    }

    putUserAttributes(data) {
        const { showBusyIndicator, showNotifyMessage } = this.props;
        showBusyIndicator(true);
        usersService
            .putUserAttributes(data)
            .then((response) => {
                // this.setState({ attributes: response.json });
                showNotifyMessage(
                    "resources.usersDataForm.successPut",
                    PrettyNotifyStyle.success
                );
                window.location.reload();
                showBusyIndicator(false);
            })
            .catch((error) => {
                console.error(
                    `Error update user attributes: ${JSON.stringify(error)}`
                );
                showNotifyMessage(
                    "resources.usersDataForm.errorPut",
                    PrettyNotifyStyle.error
                );
                showBusyIndicator(false);
            });
    }

    saveRecord = (values) => {
        let { data } = this.state;
        const { showBusyIndicator, showNotifyMessage } = this.props;
        let queryParams = shared.getUrlparams();
        showBusyIndicator(true);
        if (!Object.keys(data).length) {
            usersService
                .getUserAttributeGroups()
                .then((response) => {
                    let attrs = response.json;
                    var result = attrs.find((obj) => {
                        return obj.group === "deliveryAddress";
                    });
                    if (result && (isAdmin() || isUserAdmin())) {
                        result.mutable = true;
                    }
                    this.setState({ record: result || {} });
                })
                .catch((error) => {
                    console.error(
                        `Error getting user attributes: ${JSON.stringify(error)}`
                    );
                    showNotifyMessage(
                        "resources.usersDataForm.errorPut",
                        PrettyNotifyStyle.error
                    );
                    showBusyIndicator(false);
                })
                .finally(() => {
                    data = usersService.generateAttributeStructureWithUsername(
                        this.state.record,
                        queryParams.user
                    );
                    data.value[uuidV1()] = values["new"];
                    this.putUserAttributes(data);
                });
        } else {
            data.value[uuidV1()] = values["new"];
            this.putUserAttributes(data);
        }
    };

    inputList = () => {
        let content = [];
        let items = !this.state.itemType ? [] : this.state.itemType;
        items.forEach((item, index) => {
            content.push(
                <div key={item + index}>{Inputs.valueItem("new", item)}</div>
            );
        });
        return <div>{content}</div>;
    };

    null = () => {
        return "";
    };

    toolbar = () => {
        const {
            translate,
            save,
            push,
            showNotifyMessage,
            showBusyIndicator,
            onSave,
            submitOnEnter,
            handleSubmitWithRedirect,
            basePath,
            ...rest
        } = this.props;
        return (
            <Toolbar
                style={{
                    background: "white",
                    position: "sticky",
                    bottom: "-25px",
                    justifyContent: "flex-end",
                }}
                {...rest}
            >
                <NonInput>
                    <Button
                        key="btn-close"
                        color="primary"
                        onClick={() => this.closeDialog.bind(this)()}
                    >
                        {translate("resources.label.close")}
                    </Button>
                </NonInput>
                <SaveButton
                    label={translate("resources.label.save")}
                    redirect={false}
                    submitOnEnter={false}
                    variant="flat"
                    icon={<this.null />}
                />
            </Toolbar>
        );
    };

    render() {
        const { translate } = this.props;
        const { data } = this.state;

        return (
            <Dialog
                open={this.state.isOpen}
                maxWidth="xs"
                fullWidth={false}
                scroll="body"
                onBackdropClick={() => {
                    this.closeDialog();
                }}
                onEscapeKeyDown={() => {
                    this.closeDialog();
                }}
            >
                <DialogTitle>
                    {translate("resources.users.address.add")}
                </DialogTitle>
                {data && (
                    <DialogContent>
                        <SimpleForm
                            save={this.saveRecord.bind(this)}
                            submitOnEnter={false}
                            toolbar={this.toolbar()}
                        >
                            {this.state.itemType && <this.inputList />}
                        </SimpleForm>
                    </DialogContent>
                )}
            </Dialog>
        );
    }
}

const enhance = compose(translate);

export default enhance(
    connect(null, {
        showBusyIndicator: showBusyIndicatorAction,
        showNotifyMessage: showNotifyMessageAction,
        push: pushAction,
    })(AddressAdd)
);
