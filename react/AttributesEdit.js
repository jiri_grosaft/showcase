import React, { Component } from 'react';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import {
    translate, Show, SimpleShowLayout, TextInput, NumberInput,
    TextField, SimpleForm, Toolbar, SaveButton, BooleanInput,
    SelectInput
} from 'react-admin';
import {
    showBusyIndicator as showBusyIndicatorAction,
    showNotifyMessage as showNotifyMessageAction
} from '../../actions/uiActions';
import {
    Table, TableCell, TableRow, TableBody,
    Button, Typography, IconButton,
} from '@material-ui/core';
import * as inputTypes from "../inputTypes";
import NewItemDialog from '../components/NewItemDialog';
import IconDelete from '@material-ui/icons/Delete';
import usersService from '../services/users.service';
import { PrettyNotifyStyle, ConfirmDialog } from '../../components';
import { isAdmin, isUserAdmin } from '../../authClient';
import querystring from 'querystring';
import Inputs from "../forms/Inputs"

class AttributesEdit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            newItemDialog: false,
            attributes: null,
            itemType: null,
            record: false,
            deleteConfirm: false,
        }
    }

    valuesList = (props) => {
        const { itemType, attributes } = this.state;
        if (!itemType || !attributes) {
            return null;
        }
        const values = itemType.values;
        values.sort((a, b) => {
            return a.order - b.order;
        });
        let names = !attributes.value ? [] : Object.keys(attributes.value);
        let content = [];
        let isMutable = attributes ? attributes.mutable : false;
        names.forEach((name, index) => {
            if(attributes.group == "deliveryAddress"){
                content.push(
                    <TableRow key={name}>
                        <TableCell colSpan={2} style={{ background: '#e9e9e9' }}>
                            {isMutable &&
                                <IconButton color="primary" style={{ float: 'left', marginRight: '5px' }} onClick={() => this.removeItem.bind(this)(name)}>
                                    <IconDelete style={{ fontSize: '1.3rem' }} />
                                </IconButton>
                            }
                            <h3 style={{ fontWeight: 'bold', float: 'left' }}>
                                {"#"+(index+1)}
                            </h3>
                        </TableCell>
                    </TableRow>
                );
            } else {
                content.push(
                    <TableRow key={name}>
                        <TableCell colSpan={2} style={{ background: '#e9e9e9' }}>
                            {isMutable &&
                                <IconButton color="primary" style={{ float: 'left', marginRight: '5px' }} onClick={() => this.removeItem.bind(this)(name)}>
                                    <IconDelete style={{ fontSize: '1.3rem' }} />
                                </IconButton>
                            }
                            <h3 style={{ fontWeight: 'bold', float: 'left' }}>
                                {name}
                            </h3>
                        </TableCell>
                    </TableRow>
                );
            }
            values.forEach(item => {
                content.push(
                    <TableRow key={name + "." + item.name}>
                        <TableCell>
                            {item.label}
                            {item.desc && item.desc !== '' &&
                                <Typography variant="caption" gutterBottom>
                                    {item.desc}
                                </Typography>
                            }
                        </TableCell>
                        <TableCell>
                            {Inputs.valueItem(
                                name,
                                item,
                                attributes.value[name] ? attributes.value[name][item.name] : null
                            )}
                        </TableCell>
                    </TableRow>
                );
            });
        });
        return (
            <div>
                <Table>
                    <TableBody>
                        {content}
                    </TableBody>
                </Table>
            </div>
        );
    }

    saveRecord = values => {
        const { attributes, record } = this.state;
        attributes.mutable = record.mutable;
        attributes.visible = record.visible;
        attributes.value = values;
        this.updateAttributes(attributes);
    }

    removeItem = name => {
        this.setState({
            deleteConfirm: name
        });
    }

    removeItemConfirm = props => {
        close = () => {
            this.setState({
                deleteConfirm: false
            });
        }
        const { deleteConfirm: name } = this.state;
        if (props.type !== 'ok') {
            close();
            return;
        }
        let attrs = JSON.parse(JSON.stringify(this.state.attributes));
        if (!attrs.value[name]) {
            close();
            return;
        }
        delete attrs.value[name];
        this.updateAttributes(attrs);
    }

    updateAttributes = attrs => {
        const { showBusyIndicator, showNotifyMessage } = this.props;
        showBusyIndicator(true);
        usersService.putUserAttributes(attrs)
            .then(response => {
                // this.setState({ attributes: response.json });
                showNotifyMessage("resources.usersDataForm.successPut", PrettyNotifyStyle.success);
                window.location.reload();
            })
            .catch(error => {
                console.error(`Error update user attributes: ${JSON.stringify(error)}`);
                showNotifyMessage("resources.usersDataForm.errorPut", PrettyNotifyStyle.error);
                showBusyIndicator(false);
            })
            .finally();
    }

    initRecord = props => {
        if (this.state.record) {
            return null;
        }
        const { record } = props;
        const { showBusyIndicator } = this.props;
        if (!record) {
            return null
        }
        showBusyIndicator(true);
        this.setState({ record: JSON.parse(JSON.stringify(record)) });
        let queryParams = querystring.parse(
            location.hash
            .split("?")
            .pop()
        );
        usersService.getUserAttributes(record.group, queryParams.user)
            .then(response => {
                let attrs = response.json.pop();
                if (attrs && (isAdmin() || isUserAdmin())) {
                    attrs.mutable = true;
                }
                this.setState({ attributes: attrs || {} });
                usersService.getItemTypesByName(record.itemType)
                    .then(response => {
                        this.setState({ itemType: response.json });
                    })
                    .catch(error => console.error(`Error get item type: ${JSON.stringify(error)}`))
                    .finally(() => showBusyIndicator(false));
            })
            .catch(error => console.error(`Error get user attributes item: ${JSON.stringify(error)}`))
        return null;
    }

    openNewItemDialog = () => {
        this.setState({
            newItemDialog: true
        })
    }

    closeNewItemDialog = () => {
        this.setState({
            newItemDialog: false
        })
    }

    saveNewItemDialog = (newItemName) => {
        let { attributes } = this.state;
        if (!Object.keys(attributes).length) {
            attributes = usersService.generateAttributeStructure(this.state.record);
        } else {
            attributes.mutable = this.state.record.mutable;
            attributes.visible = this.state.record.visible;
        }
        if (newItemName) {
            attributes.value[newItemName] = {};
        }
        this.updateAttributes(attributes);
    }

    toolbar = () => {
        const { translate, hasList, hasEdit, hasCreate, hasShow, reset,
            showBusyIndicator, showNotifyMessage, refreshView, ...rest } = this.props;
        let { attributes } = this.state;
        if (!attributes.mutable || !Object.keys(attributes.value).length) {
            return null;
        }
        return (
            <Toolbar
                style={{
                    background: 'white'
                }}>
                <SaveButton
                    label={translate("resources.label.save")}
                    variant="flat"
                />
            </Toolbar>
        );
    };

    form = props => {
        return (
            <SimpleForm
                toolbar={this.toolbar()}
                save={this.saveRecord.bind(this)}
                submitOnEnter={false}>
                <this.valuesList />
            </SimpleForm>
        );
    }

    render() {
        const { showBusyIndicator, showNotifyMessage, translate, basePath, reset, ...rest } = this.props;
        const { attributes, itemType, record } = this.state;
        let title = translate('resources.users-attributes.name') + (record ? ': ' + record.group : '');
        let mutable = isAdmin() || isUserAdmin() ? true : record ? record.mutable : false;
        return (
            <div>
                {this.state.newItemDialog &&
                    <NewItemDialog
                        onClose={this.closeNewItemDialog.bind(this)}
                        onSave={this.saveNewItemDialog.bind(this)}
                        values={record.allowedValueNames}
                    />
                }
                {this.state.deleteConfirm &&
                    <ConfirmDialog closeDialog={this.removeItemConfirm.bind(this)}>
                        {translate('resources.users.confirmContinue')}
                    </ConfirmDialog>
                }
                <Show {...rest} basePath={basePath} actions={null} title={title}>
                    <SimpleShowLayout>
                        <this.initRecord />
                        <TextField source="group" />
                        <hr style={{ marginBottom: '5px' }} />
                        {mutable && record &&
                            <Button variant="outlined"
                                color="primary"
                                style={{ marginBottom: '8px' }}
                                onClick={() => { this.openNewItemDialog() }}
                                key="btn-new-item"
                            >{translate('resources.users.addNewItem')}</Button>
                        }
                        {itemType && attributes && <this.form />}
                    </SimpleShowLayout>
                </Show>
            </div>
        );
    }
}

const enhance = compose(
    translate
);

export default enhance(connect(null, {
    showBusyIndicator: showBusyIndicatorAction,
    showNotifyMessage: showNotifyMessageAction
})(AttributesEdit));